  /* eslint key-spacing:0 spaced-comment:0 */
import _debug from 'debug'
import environments from './environments'
import version from '../../config/version'
import { str } from '../modules/utils'

const debug = _debug('app:react')

// ========================================================
// Default Configuration
// ========================================================
const config = {
  version: version || '1.1.1',
  host: '/',
  env: process.env.NODE_ENV || 'development',
  backend: {
    env: str.template`/env`,
  },
  method: {
    env: 'GET',
  },
  //
}

// ========================================================
// Environment Configuration
// ========================================================
const overrides = environments[config.env]
if (overrides) {
  Object.assign(config, overrides(config))
} else {
  debug('No environment overrides found, defaults will be used.')
}

export default config
