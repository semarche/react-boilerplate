import _ from 'lodash'
import { routerActions } from 'react-router-redux'
import { UserAuthWrapper as userAuthWrapper } from 'redux-auth-wrapper'

export const userIsNotAuthenticated = userAuthWrapper({
  authSelector: state => state.auth.user,
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsNotAuthenticated',
  // Want to redirect the user when they are done loading and authenticated
  predicate: user => !user.id,
  failureRedirectPath: (state, ownProps) => ownProps.location.query.redirect || '/',
  allowRedirectBack: false,
})

export const userIsAuthenticated = userAuthWrapper({
  authSelector: state => state.auth.user,
  authenticatingSelector: state => state.auth.user.isLoading,
  // LoadingComponent: Loading,
  predicate: user => user.id,
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsAuthenticated',
})

// Admin Authorization, redirects non-admins to /app and don't send a redirect param
export const userIsAdmin = userAuthWrapper({
  authSelector: state => state.auth.user,
  redirectAction: routerActions.replace,
  failureRedirectPath: '/images',
  wrapperDisplayName: 'UserIsAdmin',
  predicate: user => user.isAdmin,
  allowRedirectBack: false,
})

export const authOrNot = (Component, FailureComponent) => userAuthWrapper({
  authSelector: state => state.auth.user,
  wrapperDisplayName: 'authOrNot',
  predicate: user => !!user.id,
  FailureComponent,
})(Component)
