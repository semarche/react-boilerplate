const has = (o, property) => Object.prototype.hasOwnProperty.call(o, property)
const getKeyValue = (o, property, defaultValue) => (o !== undefined && has(o, property) ? o[property] : defaultValue)
const get = (o, property, defaultValue) => (o !== undefined && has(o, property) ? o[property] : defaultValue)
const isEmpty = (o) => !(o.constructor === Object && Object.keys(o).length > 0)
const isArray = (a) => a && Array.isArray(a)
const getArrValue = (a, index, defaultValue) => (typeof a[index] !== 'undefined' ? a[index] : defaultValue)
const objByPages = (a, rows) => {
  const inRows = (rows === 0 ? 10 : rows)
  return isArray(a) ? a.reduce(
    (n, t, i) => {
      let j = Math.trunc(i / inRows);
      (n[j] || (n[j] = [])).push(t)
      return n
    },
    {}
  ) : {}
}
const objByKey = (a, key) => (isArray(a) ? a.reduce(
    (n, t) => {
      (n[t[key]] || (n[t[key]] = [])).push(t)
      return n
    },
    {}
  ) : {}
)

const isObject = a => (typeof a === 'object' && a !== null)

const deepGet = (obj, props, defaultValue) => {
  // If we have reached an undefined/null property
  // then stop executing and return the default value.
  // If no default was provided it   will be undefined.
  if (obj === undefined || obj === null) {
    return defaultValue
  }

  // If the path array has no more elements, we've reached
  // the intended property and return its value
  if (props.length === 0) {
    return obj
  }

  // Prepare our found property and path array for recursion
  const foundSoFar = obj[props[0]]
  const remainingProps = props.slice(1)

  return deepGet(foundSoFar, remainingProps, defaultValue)
}

const toUTCString = (time) => {
  const d = new Date()
  return time && d.setTime(time) ? d.toUTCString() : '-'
}

const template = (strings, ...keys) => (...values) => {
  const dict = getArrValue(values, [values.length - 1], {})
  return strings.reduce(
    (accumulator, part, i) => accumulator
      + (Number.isInteger(keys[i - 1]) ? getArrValue(values, keys[i - 1], '') : getKeyValue(dict, keys[i - 1], ''))
      + part
  )
}

const makeBasicAuth = (n, p) => `Basic ${btoa(`${n}:${p}`)}`

const ucFirst = (string) => string.charAt(0).toUpperCase() + string.slice(1)

const search = (str1, str2) => ((str1 !== undefined && str2 !== undefined) ? String(str1.toLowerCase()).includes(String(str2.toLowerCase())) : false)

const getServiceVersion = str => str.image.split(/[:]+/).pop()
const getServiceName = str => str.image.split(/[/]+/).pop().split(/[:]+/).shift()

const short = (a, b) => {
  a = a.toString()
  b = b.toString()
  let n = 0
  let i = 0
  for (i = 0, n = Math.max(a.length, b.length); i < n && a.charAt(i) === b.charAt(i); ++i) {
  }
  if (i === n) return 0
  return a.charAt(i) > b.charAt(i) ? -1 : 1
}

const cmp = (a, b) => {
  if (a < b) return -1
  if (a > b) return 1
  return 0
}

export const cast = (fields, props) => {
  const item = Object.keys(fields)
    .reduce(
      (acc, val) => set(acc, val, get(props, val, fields[val])),
      {}
    )
  if (has(props, '_id')) {
    item._id = get(props, '_id', 0)
  }
  return item
}

const objParse = row => {
  let parsedData = Object.assign({}, row)
  Object.keys(row).forEach(key => {
    if (isObject(row[key])) {
      const rowObjectFields = {}
      Object.keys(row[key]).forEach(ob => {
        rowObjectFields[`${key}-${ob}`] = row[key][ob]
      })
      parsedData = Object.assign(parsedData, rowObjectFields)
    }
  })
  return parsedData
}

const getActive = (arr, key) => {
  const filtred = arr.filter(f => (f.active === 'true'))
  return filtred.length ? filtred[0][key] : ''
}

const group = (s, a) => {
  const grouped = []
  a.forEach((f, i) => {
    if (i === 0) {
      return grouped.push(f)
    }
    const temp = {}
    s.forEach(k => {
      if (k.grouped && f[k.key] === a[i - 1][k.key]) {
        temp[k.key] = null
      } else {
        temp[k.key] = f[k.key]
      }
    })
    return grouped.push(temp)
  })
  return grouped
}

const set = (o, property, value) => {
  // const  = { ...o }
  o[property] = value
  return o
}

// const result = [strings[0]]
// keys.forEach((key, i) => {
//   const value = Number.isInteger(key) ? values[key] : dict[key]
//   result.push(value, strings[i + 1])
// })
// return result.join('')
// return template.reduce((accumulator, part, i) => {
//   return accumulator + expressions[i - 1] + part
// })

export const obj = { get, set, getKeyValue, deepGet, has, isEmpty, isObject, objParse, cast }
export const arr = { isArray, objByKey, getArrValue, objByPages, group, getActive }
export const str = { template, cmp, ucFirst, short, search, makeBasicAuth, getServiceVersion, getServiceName }
export const date = { toUTCString }
export default { obj, arr, str, date }
