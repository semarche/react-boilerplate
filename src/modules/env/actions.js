import { CALL_API } from 'redux-api-middleware'

import { consts } from './consts'
import appConfig from '../../config'
// ------------------------------------
// Constants
// ------------------------------------
const host = appConfig.host

// ------------------------------------
// Actions
// ------------------------------------
export const fetchEnv = () => (dispatch, getState) => {
  console.log('fetchEnv')
  if (getState().env.isFetching) {
    return Promise.resolve()
  }
  return dispatch({
    [CALL_API]: {
      types: [
        consts.FETCH_ENV_REQUEST,
        consts.FETCH_ENV_SUCCESS,
        consts.FETCH_ENV_FAILURE,
      ],
      endpoint: appConfig.backend.env({ host }),
      method: appConfig.method.env,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
  })
}

export const actions = {
  fetchEnv,
}

export default actions
