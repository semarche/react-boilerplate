import { createSelector } from 'reselect'
import { obj } from '../utils'
import { consts } from './consts'
import { initial } from './initial'

export const getVars = state => obj.deepGet(state, [consts.KEY, 'vars'], initial.vars)
export const getBackend = createSelector(
  [getVars],
  a => obj.get(a, 'backend', '')
)
export const getIsFetching = state => obj.deepGet(state, [consts.KEY, 'isFetching'], false)

export default {
  getVars,
  getBackend,
  getIsFetching,
}
