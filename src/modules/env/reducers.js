import { arr, obj } from '../utils'
// ------------------------------------
// Constants
// ------------------------------------
import { consts } from './consts'
// ------------------------------------
// Initial State
// ------------------------------------
import { initial } from './initial'
// ------------------------------------
// Action Handlers
// ------------------------------------
export const actionHandlers = {
  [consts.FETCH_ENV_REQUEST]: (state, action) => {
    console.log('FETCH_ENV_REQUEST')
    return { ...state, isFetching: true }
  },
  [consts.FETCH_ENV_FAILURE]: (state, action) => ({ ...state, isFetching: false }),
  [consts.FETCH_ENV_SUCCESS]: (state, action) => {
    const result = { ...state, isFetching: false }
    console.log('FETCH_ENV_SUCCESS', action)
    if (obj.isObject(action.payload)) {
      result.vars = obj.cast(state.vars, action.payload)
    }
    return result
  },
}

export const reducers = (state = initial, action) => {
  const handler = actionHandlers[action.type]
  return handler ? handler(state, action) : state
}

export default reducers
