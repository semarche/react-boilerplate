// ------------------------------------
// Constants
// ------------------------------------
export const consts = {
  KEY: 'env',

  FETCH_ENV_REQUEST: 'env.FETCH_ENV_REQUEST',
  FETCH_ENV_SUCCESS: 'env.FETCH_ENV_SUCCESS',
  FETCH_ENV_FAILURE: 'env.FETCH_ENV_FAILURE',

}

export default consts
