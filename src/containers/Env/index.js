import { connect } from 'react-redux'

import { fetchEnv } from '../../modules/env/actions'
import { getBackend, getIsFetching } from '../../modules/env/selectors'

import { Index } from '../../components/Env'

const mapActionCreators = {
  fetchEnv,
}

const mapStateToProps = state => ({
  isFetching: getIsFetching(state),
  backend: getBackend(state),
})

export default connect(mapStateToProps, mapActionCreators)(Index)
