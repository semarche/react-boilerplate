import React from 'react'
import PropTypes from 'prop-types'
import { Router } from 'react-router'
import { Provider } from 'react-redux'
import Helmet from 'react-helmet'
import clone from 'clone'

import defaultLayout from '../../config/layout'
import Env from './Env'

const propTypes = {
  layout: PropTypes.object,
  history: PropTypes.object.isRequired,
  routes: PropTypes.object.isRequired,
  routerKey: PropTypes.number,
  store: PropTypes.object.isRequired,
}

const defaultProps = {
  layout: defaultLayout,
  routerKey: 0,
}

export const AppContainer = ({ layout, history, routes, routerKey, store }) => (
  <Provider store={store}>
    <div>
      <Helmet {...Object.assign(clone(defaultLayout), layout)} />
      <Env />
      <Router history={history} key={routerKey}>
        {routes}
      </Router>
    </div>
  </Provider>
)

AppContainer.propTypes = propTypes
AppContainer.defaultProps = defaultProps
export default AppContainer
