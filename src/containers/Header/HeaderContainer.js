import { connect } from 'react-redux'
import { Header } from '../../components/Header/Header'
import { toggleDocs } from '../../routes/Home/modules/index'

const mapDispatchToProps = {
  toggleDocs,
}

const mapStateToProps = (state) => ({
  showDocs: state.home.showDocs,
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
