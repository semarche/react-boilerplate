import React from 'react'
import PropTypes from 'prop-types'
import { IndexLink, Link } from 'react-router'
import { Navbar, Image, Button } from 'react-bootstrap'
import classnames from 'classnames'
import styles from './Header.scss'

// import { authOrNot } from '../../modules/Auth'
import { UserSigned } from './UserSigned'
import { UserUnsigned } from './UserUnsigned'

const propTypes = {
  title: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
}

const defaultProps = {
  title: 'React boilerplate',
  user: { id: null },
}

export const Navigation = (props) => {

  // console.log(props)
  return (
    <Navbar fluid className={classnames(styles.navbar)}>
      <Navbar.Header>
        <Navbar.Brand>
          <IndexLink to='/'>
            <Image src='' className={styles.icon}/>
          </IndexLink>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Navbar.Text>
          <strong>{props.title}</strong>
        </Navbar.Text>
        <Navbar.Text pullRight>
          { props.user.id ? <UserSigned user={props.user} setSignOut={props.setSignOut}/> : <UserUnsigned/> }
        </Navbar.Text>
      </Navbar.Collapse>

    </Navbar>
  )
}

Navigation.propTypes = propTypes
Navigation.defaultProps = defaultProps
export default Navigation
