import React from 'react'
import { Link } from 'react-router'

const propTypes = {}

const defaultProps = {}

export const UserUnsigned = () => {
  return (<Link to='/login'>Sign In</Link>)
}

UserUnsigned.propTypes = propTypes
UserUnsigned.defaultProps = defaultProps
export default UserUnsigned
