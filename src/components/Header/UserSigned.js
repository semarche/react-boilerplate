import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

import { userEmpty } from '../../routes/Login/modules'

const propTypes = {
  user: PropTypes.object,
  setSignOut: PropTypes.func,
}

const defaultProps = {
  logged: false,
  user: userEmpty,
}

export const UserSigned = (props) => {
  return (<span>
    {props.user.title + ' '}
    <Link to='/logout' onClick={() => {
      props.setSignOut()
      return false
    }}>Sign Out</Link>
  </span>)
}

UserSigned.propTypes = propTypes
UserSigned.defaultProps = defaultProps
export default UserSigned
