import React from 'react'
import PropTypes from 'prop-types'
import { Navigation } from './Navigation'

const propTypes = {

}
const defaultProps = {
  showDocs: false,
}

export const Header = (props) => (
  <Navigation />
)

Header.propTypes = propTypes
Header.defaultProps = defaultProps
export default Header
