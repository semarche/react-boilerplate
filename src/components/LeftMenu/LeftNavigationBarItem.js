import React from 'react'
import PropTypes from 'prop-types'
import PureComponent from 'react-pure-render/component'
import { VelocityComponent } from 'velocity-react'
import classes from './leftnavigation.scss'
import { LinkIcon } from './LinkIcon'
import { LinkText } from './LinkText'

const propTypes = {
  expanded: PropTypes.bool,
  path: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  title: PropTypes.string,
  active: PropTypes.bool,
  hover: PropTypes.bool,
  disabled: PropTypes.bool,
}

const defaultProps = {
  expanded: false,
  path: '#',
  icon: '',
  title: '',
  active: false,
  hover: false,
  disabled: false,
}

export class LeftNavigationBarItem extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      active: props.active,
      hover: props.hover,
    }
  }

  render() {
    const props = this.props
    const state = this.state
    const indicatorDuration = 300
    const titleDuration = 300
    const indicatorAnimation = {
      opacity: state.active ? 1 : 0,
      height: state.active ? 60 : 0,
      top: state.active ? 0 : 30,
    }
    const titleAnimation = {
      left: props.expanded ? 70 : -100,
      opacity: props.expanded ? 1 : 0,
    }
    return (
      <div
        style={props.disabled ? { background: "rgba(193, 185, 185, 0.08)" } : {}}
        className={classes.menuRow}
        onMouseEnter={() => {
          this.setState({ active: true })
        }}
        onMouseLeave={() => {
          this.setState({ active: false })
        }}
      >
        <VelocityComponent
          key={'AnimIcon'}
          animation={indicatorAnimation}
          duration={indicatorDuration}
        >
          <div className={classes.menuIndicator}/>
        </VelocityComponent>
        <div className={classes.menuIcon}>
          <LinkIcon path={props.path} icon={props.icon} title={props.title} disabled={props.disabled}/>
        </div>
        <VelocityComponent
          key={'titleAnim'}
          animation={titleAnimation}
          duration={titleDuration}
        >
          <div className={classes.menuLink}>
            <LinkText path={props.path} icon={props.icon} title={props.title} disabled={props.disabled}>
              {props.children}
            </LinkText>
          </div>
        </VelocityComponent>
      </div>
    )
  }
}

LeftNavigationBarItem.propTypes = propTypes
LeftNavigationBarItem.defaultProps = defaultProps
export default LeftNavigationBarItem
