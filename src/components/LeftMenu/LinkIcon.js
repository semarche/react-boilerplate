import React from 'react'
import PropTypes from 'prop-types'

import { Link } from 'react-router'

const propTypes = {
  path: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  title: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  disabled: PropTypes.bool,
}

const defaultProps = {
  title: '',
  width: 24,
  height: 24,
  disabled: false,
}

export const LinkIcon = props => (
  <Link to={props.disabled ? null : props.path} title={props.title}>
    <img src={props.icon} alt={props.title} width={props.width} height={props.height} />
  </Link>
)

LinkIcon.propTypes = propTypes
LinkIcon.defaultProps = defaultProps
export default LinkIcon
