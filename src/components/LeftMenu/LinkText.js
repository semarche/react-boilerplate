import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const propTypes = {
  path: PropTypes.string.isRequired,
  title: PropTypes.string,
  children: PropTypes.any,
  disabled: PropTypes.bool,
}

const defaultProps = {
  title: '',
  children: null,
  disabled: false,
}

export const LinkText = props => (
  <Link
    to={props.disabled ? null : props.path}
    title={props.title}
    style={props.disabled ? { cursor: 'default' } : {}}
  >
    {props.children}
  </Link>
)

LinkText.propTypes = propTypes
LinkText.defaultProps = defaultProps
export default LinkText
