import React from 'react'
import PropTypes from 'prop-types'
import PureComponent from 'react-pure-render/component'
import { VelocityComponent } from 'velocity-react'
// import Sortable from 'react-sortablejs'
import classes from './leftnavigation.scss'
import { LeftNavigationBarItem } from './LeftNavigationBarItem'
import menuItems from '../../config/menu.json'

const propTypes = {
  expanded: PropTypes.bool,
}
const defaultProps = {
  expanded: false,
}
export class LeftNavigationBar extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      expanded: props.expanded,
    }
    this.bindAll('setExpand', 'setShrink')
  }

  setExpand() {
    this.setState({ expanded: true })
  }

  setShrink() {
    this.setState({ expanded: false })
  }

  bindAll(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this)
    })
  }

  render() {
    const containerDuration = 300
    const containerAnimation = {
      width: this.state.expanded ? 200 : 100,
    }

    return (
      <VelocityComponent
        animation={containerAnimation}
        duration={containerDuration}
      >
        <div
          key='container' className={classes.container}
          onMouseEnter={this.setExpand}
          onMouseLeave={this.setShrink}
        >
          <div key={'items'} className={classes.items}>
            { menuItems.map((t, index) => (
              <LeftNavigationBarItem key={`menuItem_${index}`} {...t} expanded={this.state.expanded}>
                {t.title}
              </LeftNavigationBarItem>
            )) }
          </div>
          <div key={'filler'} className={classes.filler}>
            {' '}
          </div>
        </div>
      </VelocityComponent>
    )
  }

}

LeftNavigationBar.propTypes = propTypes
LeftNavigationBar.defaultProps = defaultProps
export default LeftNavigationBar
