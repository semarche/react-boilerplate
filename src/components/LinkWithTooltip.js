import React from 'react'
import PropTypes from 'prop-types'
import { Tooltip, OverlayTrigger } from 'react-bootstrap'
import { Link } from 'react-router'

const propTypes = {
  id: PropTypes.string.isRequired,
  tooltip: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
}

const defaultProps = {}

export const LinkWithTooltip = (props) => {
  const tooltip = <Tooltip id={props.id}>{props.tooltip}</Tooltip>

  return (
    <OverlayTrigger
      overlay={tooltip} placement='top'
    >
      <Link to={props.href}>{props.children}</Link>
    </OverlayTrigger>
  );
}

LinkWithTooltip.propTypes = propTypes
LinkWithTooltip.defaultProps = defaultProps
export default LinkWithTooltip
