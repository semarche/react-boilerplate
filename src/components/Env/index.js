import React from 'react'
import { PropTypes } from 'prop-types'

const propTypes = {
  fetchEnv: PropTypes.func.isRequired,
  // isFetching: PropTypes.bool,
  // backend: PropTypes.string,
}
const defaultProps = {
  // isFetching: false,
  // backend: '',
}

export class Index extends React.Component {

  componentDidMount() {
    this.props.fetchEnv()
    console.log('render env component')
  }

  render() {
    console.log('env', this.props)
    return null
  }
}

Index.propTypes = propTypes
Index.defaultProps = defaultProps
export default Index
