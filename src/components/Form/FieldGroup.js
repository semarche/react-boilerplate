
import React from 'react'
import PropTypes from 'prop-types'
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap'

export const FieldGroup = ({ id, label, ...props }) =>
  <FormGroup controlId={id}>
    <ControlLabel>{label}</ControlLabel>
    <FormControl {...props} />
  </FormGroup>

FieldGroup.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
}

export default FieldGroup
