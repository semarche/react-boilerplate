import thunk from 'redux-thunk'
import { applyMiddleware, compose, createStore } from 'redux'
import { persistStore, autoRehydrate } from 'redux-persist'
// import crosstabSync from 'redux-persist-crosstab'
import { routerMiddleware } from 'react-router-redux'
import { loadingBarMiddleware } from 'react-redux-loading-bar'
import { apiMiddleware } from 'redux-api-middleware'
import { makeRootReducer } from './reducers'

// ======================================================
// web socket Configuration
// ======================================================
// const socket = io(socketConf)
// const socketIoMiddleware = createSocketIoMiddleware(socket, 'socket/')

export default (initialState = {}, history) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [
    thunk,
    routerMiddleware(history),
    apiMiddleware,
    loadingBarMiddleware({
      promiseTypeSuffixes: ['REQUEST', 'SUCCESS', 'FAILURE'],
    }),
  ]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []
  if (__DEBUG__ && typeof window !== 'undefined' && window) {
    const devToolsExtension = window.devToolsExtension
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(
      autoRehydrate(),
      applyMiddleware(...middleware),
      ...enhancers,
    )
  )

  const persistor = persistStore(store, { whitelist: ['auth'] })
  // crosstabSync(persistor, { whitelist: ['auth'] })

  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./reducers', (t) => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
