import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import { loadingBarReducer } from 'react-redux-loading-bar'

import { initialState as HomeInitialState, homeReducer } from '../routes/Home/modules'
import envReducers from '../modules/env/reducers'
import envInitial from '../modules/env/initial'
import imagesInitial from '../routes/Images/modules/initial'

// Fix: "React-Redux: Combining reducers: Unexpected Keys"
// http://stackoverflow.com/a/33678198/789076
const initialReducers = {
  home: (state = HomeInitialState) => state,
  env: (state = envInitial) => state,
  images: (state = imagesInitial) => state,
}

export const makeRootReducer = asyncReducers => combineReducers({
  // Add sync reducers here
  router,
  loadingBar: loadingBarReducer,
  ...initialReducers,
  home: homeReducer,
  env: envReducers,
  ...asyncReducers,
})

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
