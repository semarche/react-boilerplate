/* eslint key-spacing:0 spaced-comment:0 */
import _debug from 'debug'
import environments from './environments'

const debug = _debug('app:react')

// ========================================================
// Default Configuration
// ========================================================
const config = {
  env : process.env.NODE_ENV || 'development',
  sso: {
    loginRedirect: '/auth/login',
    logoutRedirect: '/auth/logout',
  },
  //
}

// ========================================================
// Environment Configuration
// ========================================================
const overrides = environments[config.env]
if (overrides) {
  Object.assign(config, overrides(config))
} else {
  debug('No environment overrides found, defaults will be used.')
}

export default config
