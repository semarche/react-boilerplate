import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { Grid, Row, Panel, Button, FormGroup, ControlLabel, FormControl, Col } from 'react-bootstrap'
import classnames from 'classnames'
import styles from '../styles/index.scss'
import config from '../../../config'
import { routerActions } from 'react-router-redux'

import { userEmpty, userLogged } from '../modules'

const propTypes = {
  setTitle: PropTypes.func.isRequired,
  setSignIn: PropTypes.func.isRequired,
  user: PropTypes.object,
}

const defaultProps = {}
export class Index extends Component {

  constructor(props) {
    super(props)

    const user = props.user === null ? userEmpty : props.user
    this.state = {
      user,
    }

    this.bindAll('submit', 'handleChange')
  }

  bindAll(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this)
    })
  }

  handleChange(e) {
    // const user = this.state.user
    // user[e.target.name] = e.target.datatype === 'number' ? parseFloat(e.target.value) : e.target.value
    // this.setState({ user })
  }

  submit() {
    this.props.setSignIn(userLogged)
  }

  render() {
    this.props.setTitle('Sing In')
    return (
      <div className={styles.panelWrapper}>
        <Helmet title='Sign In'/>
        <Panel bsClass={classnames('panel', styles.panel)}>
          <div className={classnames('container-fluid', 'text-center')}>
            <div className={styles.logo}>
              <img src='/assets/login/u19.png'/>
              <h1>SP Analytics</h1>
            </div>
            <FormGroup controlId='loginEmal' bsClass={classnames('form-group',styles.formGroup)}>
              {/* <ControlLabel>Sign in</ControlLabel> */}
              <FormControl
                type='text' name='login' placeholder='Email'
                autoComplete='off' value={this.state.email} onChange={this.handleChange}
              />
            </FormGroup>

            <Button
              bsStyle='primary'
              bsClass={classnames('rounded', 'btn')}
              onClick={() => this.submit()}
            >
              Sign in
            </Button>
            {/*
             <div className='form-group'>
             <input className='form-input' value={this.state.email} onChange={this.updateEmail} placeholder='Email'
             autoComplete='off'/>
             </div>

             <div className='form-group'>
             <input type='password' value={this.state.password} onChange={this.updatePassword}
             placeholder='Password' autoComplete='off'/>
             </div>

             <div className='form-group-footer'>
             <div className='remember-me-container'>
             <input type='checkbox' checked={this.state.rememberMe} onChange={this.updateRememberMe}/>
             <label>Remember me</label>
             </div>

             <div className='forgot-password'>
             <a href='#'>Forget ID/Password</a>
             </div>
             </div>

             <div className='buttons'>
             <div className='button'>
             <span>Login</span>
             </div>
             </div>

             <FormGroup controlId='loginEmal'>
             <ControlLabel>Sign in</ControlLabel>
             <FormControl type='text' name='login'/>
             </FormGroup>

             <Button
             bsStyle='primary'
             bsClass={classnames('rounded', 'btn')}
             onClick={() => this.submit()}
             >
             Sign in
             </Button>
             */}
          </div>
        </Panel>
      </div>
    )
  }
}

Index.propTypes = propTypes
Index.defaultProps = defaultProps
export default Index
