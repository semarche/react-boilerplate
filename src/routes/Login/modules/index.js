import { routerActions } from 'react-router-redux'

import config from '../config'
// ------------------------------------
// Constants
// ------------------------------------
export const consts = {
  SET_SIGNOUT: 'AUTH.SET_SIGNOUT',
  SET_SIGNIN: 'AUTH.SET_SIGNIN',
}

// ------------------------------------
// Actions
// ------------------------------------
export function setSignOutSync() {
  return {
    type: consts.SET_SIGNOUT,
    payload: userEmpty,
  }
}

export function setSignInSync(user) {
  return {
    type: consts.SET_SIGNIN,
    payload: user,
  }
}

/*  This is a thunk, meaning it is a function that immediately
 returns a function for lazy evaluation. It is incredibly useful for
 creating async actions, especially when combined with redux-thunk!

 NOTE: This is solely for demonstration purposes. In a real application,
 you'd probably want to dispatch an action of COUNTER_DOUBLE and let the
 reducer take care of this logic.  */

export const setSignOut = value => dispatch => new Promise(resolve => {
  setTimeout(() => {
    dispatch(setSignOutSync(value))
    resolve()
  }, 100)
  setTimeout(() => {
    window.location = config.sso.logoutRedirect
  }, 20)
})

export const setSignIn = value => dispatch => new Promise(resolve => {
  setTimeout(() => {
    dispatch(setSignInSync(value))
    resolve()
  }, 100)
  setTimeout(() => {
    window.location = config.sso.loginRedirect
  }, 20)
})

export const actions = {
  setSignOut,
  setSignIn,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [consts.SET_SIGNIN]: (state, action) => ({ ...state, user: action.payload, logged: true }),
  [consts.SET_SIGNOUT]: (state, action) => ({ ...state, user: action.payload, logged: false }),
}

// ------------------------------------
// Reducer
// ------------------------------------
export const userLogged = { id: 190, email: 'vvvl@yoursite.com', title: 'John Doe', isAdmin: true }
export const userEmpty = { id: 0, email: '', title: '', isAdmin: false }
export const initialState = { logged: false, user: userEmpty }
export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
