import { injectReducer } from '../../store/reducers'
// import { userIsAuthenticated } from '../../modules/Auth'

export const createRoutes = store => ({
  path: 'images',

  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      const consts = require('./modules/consts').default
      const Container = require('./containers/IndexContainer').default
      const reducer = require('./modules/reducers').default
      injectReducer(store, { key: consts.KEY, reducer })
      callback(null, Container)
    }, 'images')
  },

  /**
   * or connect with home container
   *
   * @param partialNextState
   * @param callback
   */
  getIndexRoute(partialNextState, callback) {
    require.ensure([], (require) => {
      callback(null, {
        component: require('./routes/List/containers/ImagesListContainer').default,
      })
    }, 'images.list')
  },

  /**
   * hardcoded redirect
   */
  // indexRoute: {
  //   onEnter: (nextState, replace) => replace('/monuments'),
  // },

  getChildRoutes(partialNextState, callback) {
    require.ensure([], (require) => {
      callback(null, [
        require('./routes/Info').default(store),
      ])
    }, 'images.childs')
  },

})

export default createRoutes
