import React from 'react'
import PropTypes from 'prop-types'

import classnames from 'classnames'
import { obj } from '../../../modules/utils'

const propTypes = {
  children: PropTypes.any,
  page: PropTypes.string.isRequired,
}

const defaultProps = {
  showDocs: false,
  page: 'images',
}

export const Index = ({children}) => (
  <div>
    <div className='container'>
      {children}
    </div>
  </div>
)

Index.propTypes = propTypes
Index.defaultProps = defaultProps
export default Index
