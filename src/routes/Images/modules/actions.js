import { CALL_API } from 'redux-api-middleware'

import imagesConfig from '../config'
import consts from './consts'

const host = imagesConfig.apihost
// ------------------------------------
// Actions
// ------------------------------------
export const setQ = (q) => (dispatch, getState) => {
  return dispatch({ type: consts.SET_Q, payload: q })
}

export const setP = (p) => (dispatch, getState) => {
  return dispatch({ type: consts.SET_P, payload: p })
}

export const setPageRows = (n) => (dispatch, getState) => {
  return dispatch({ type: consts.SET_PAGE_ROWS, payload: n })
}

export const setId = (id) => (dispatch, getState) => {
  return dispatch({ type: consts.SET_ID, payload: id })
}

export const fetchImages = (count, query) => (dispatch, getState) => {
  if (getState().images.isFetching) {
    return Promise.resolve()
  }
  return dispatch({
    [CALL_API]: {
      types: [
        consts.FETCH_DATA_REQUEST,
        consts.FETCH_DATA_SUCCESS,
        consts.FETCH_DATA_FAILURE,
      ],
      endpoint: imagesConfig.backend.fetchImages({ host, count, query }),
      method: imagesConfig.method.fetchImages,
      headers: { Accept: '*/*' },
    },
  })
}

export const actions = {
  setQ,
  setP,
  setPageRows,
  setId,
  fetchImages,
}

export default actions
