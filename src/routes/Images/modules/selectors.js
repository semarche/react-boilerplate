import { createSelector } from 'reselect'
import { arr, obj } from '../../../modules/utils'
import {consts} from './consts'

export const getImageInfo = (state) => state[consts.KEY].imageInfo

export const getId = state => obj.deepGet(state, [consts.KEY, 'Id'], 0)
export const getQ = state => obj.deepGet(state, [consts.KEY, 'q'], '')
export const getP = state => parseInt(obj.deepGet(state, [consts.KEY, 'p'], 1), 10)
export const getPageRows = state => parseInt(obj.deepGet(state, [consts.KEY, 'pageRows'], 10), 10)

export const getIsFetching = (state) => state[consts.KEY].isFetching
export const getData = (state) => state[consts.KEY].images
export const getDataCount = createSelector([getData], a => {
  return a == null ? 0 : a.length
})

export const getDataFiltered = createSelector(
  [getQ, getData],
  (q, data) => (q.length ? data.filter(t => t.repository.includes(q)) : data)
)
export const getDataFilteredLength = createSelector([getDataFiltered], a => {
  return a == null ? 0 : a.length
})

export const getDataPaged = createSelector(
  [getPageRows, getDataFiltered],
  (n, data) => arr.objByPages(data, n)
)

export const getDataOnPage = createSelector(
  [getP, getDataPaged],
  (p, data) => obj.getKeyValue(data, p - 1)
)
export const selectDataItem = createSelector(
  [getId, getData],
  (id, items) => items.find(t => id === parseInt(t.id, 10))
)

export default {
  getId,
  getQ,
  getP,
  getPageRows,
  getIsFetching,
  getData,
  getDataCount,
  getDataFiltered,
  getDataFilteredLength,
  getDataPaged,
  getDataOnPage,
  selectDataItem,

  getImageInfo,
}
