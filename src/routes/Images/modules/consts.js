// ------------------------------------
// Constants
// ------------------------------------
export const consts = {
  KEY: 'images',
  SET_ID: 'images.SET_ID',
  SET_Q: 'images.SET_Q',
  SET_P: 'images.SET_P',
  SET_PAGE_ROWS: 'images.SET_PAGE_ROWS',

  FETCH_DATA_REQUEST: 'images.FETCH_DATA_REQUEST',
  FETCH_DATA_SUCCESS: 'images.FETCH_DATA_SUCCESS',
  FETCH_DATA_FAILURE: 'images.FETCH_DATA_FAILURE',
}

export default consts
