import environments from './environments'
import { str } from '../../../modules/utils'
// ========================================================
// Default Configuration
// ========================================================
const config = {
  env: process.env.NODE_ENV || 'development',
  dataLive: false,
  apihost: '/api',
  backend: {
    fetchImages: str.template`${'host'}/images/find?provider=dockerRegistry&count=${'count'}&q=${'query'}`,
    fetchImageInfo: str.template`${'host'}/??/${'image'}/??`,
  },
  method: {
    fetchImages: 'GET',
    fetchImageInfo: 'GET',
  },
}

// ========================================================
// Environment Configuration
// ========================================================
const overrides = environments[config.env]
if (overrides) {
  Object.assign(config, overrides(config))
}

export default config
