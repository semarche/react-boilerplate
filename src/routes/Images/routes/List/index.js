// import { injectReducer } from '../../../../store/reducers'

// import { userIsAuthenticated } from '../../modules/Auth'

export default (store) => ({
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Container = require('./containers/ImagesListContainer').default
      // const reducer = require('../../modules').default

      /*  Add the reducer to the store on key 'Applications'  */
      // injectReducer(store, { key: 'applications', reducer })

      /*  Return getComponent   */
      // cb(null, userIsAuthenticated(Store))
      cb(null, Container)

    /* Webpack named bundle   */
    }, 'imagesList')
  },
})
