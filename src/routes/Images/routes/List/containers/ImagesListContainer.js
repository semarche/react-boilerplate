import { connect } from 'react-redux'
import { actions } from '../../../modules/actions'
import { setTitle, goTo } from '../../../../Home/modules'
import {
  getQ,
  getP,
  getPageRows,
  getIsFetching,
  getDataFilteredLength,
  getDataOnPage,
} from '../../../modules/selectors'

/*  This is a container component. Notice it does not contain any JSX,
 nor does it import React. This component is **only** responsible for
 wiring in the actions and state necessary to render a presentational
 component - in this case, the counter:   */

import { Images } from '../components/Images'

/*  Object of action creators (can also be function that returns object).
 Keys will be passed as props to presentational components. Here we are
 implementing our wrapper around increment; the component doesn't care   */

const mapActionCreators = {
  setTitle,
  goTo,
  ...actions,
}

const mapStateToProps = (state) => {
  const res = {
    page: 'images',
    items: getDataOnPage(state),
    itemsCount: getDataFilteredLength(state),
    p: getP(state),
    q: getQ(state),
    pageRows: getPageRows(state),
    isFetching: getIsFetching(state),
  }

  return res
}

export default connect(mapStateToProps, mapActionCreators)(Images)
