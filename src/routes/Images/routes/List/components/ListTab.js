import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'react-bootstrap'


const propTypes = {
  images: PropTypes.array,
}

const defaultProps = {
  images: [],
}

export const ListTab = ({ images }) => (
  <Table hover>
    <thead>
      <tr>
        <th width='25%' key='name' label='Name'>
          <span className='sort-toggle clickable'>
            Account
            <a><span className='glyphicon glyphicon-Down-triangle' /></a>
          </span>
        </th>
        <th width='30%' key='createTs' label='Created'>
          <span className='sort-toggle clickable inactive-sort-key'>
            Registry
            <a><span className='glyphicon glyphicon-Down-triangle' /></a>
          </span>
        </th>
        <th width='30%' key='updateTs' label='Updated'>
          <span className='sort-toggle clickable inactive-sort-key'>
            Repository
            <a><span className='glyphicon glyphicon-Down-triangle' /></a>
          </span>
        </th>
        <th width='15%' key='email' label='Owner'>
          <span className='sort-toggle clickable inactive-sort-key'>
            Tag
            <a><span className='glyphicon glyphicon-Down-triangle' /></a>
          </span>
        </th>
      </tr>
    </thead>
    <tbody>
      {images.map((t, i) => (<tr key={`app_${i}`} className='clickable'>
        <td>
          {t.account}
        </td>
        <td>{t.registry}</td>
        <td>{t.repository}</td>
        <td>{t.tag}</td>
      </tr>)) }
    </tbody>
  </Table>
)

ListTab.propTypes = propTypes
ListTab.defaultProps = defaultProps
export default ListTab
