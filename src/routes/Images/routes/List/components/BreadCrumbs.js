import React from 'react'
import { Breadcrumb } from 'react-bootstrap'
import { IndexLinkContainer } from 'react-router-bootstrap'

const propTypes = {}

const defaultProps = {}

export const BreadCrumbs = () => (
  <Breadcrumb>
    <IndexLinkContainer to='/'>
      <Breadcrumb.Item href='/'>
        Dashboard
      </Breadcrumb.Item>
    </IndexLinkContainer>
    <Breadcrumb.Item active>
      Images
    </Breadcrumb.Item>
  </Breadcrumb>
)

BreadCrumbs.propTypes = propTypes
BreadCrumbs.defaultProps = defaultProps
export default BreadCrumbs
