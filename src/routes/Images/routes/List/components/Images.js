import React, { Component } from 'react'
import Helmet from 'react-helmet'
import Pagination from 'react-js-pagination'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import classnames from 'classnames'
import {
  Grid, Row, Col, FormGroup, FormControl,
  DropdownButton, MenuItem, Glyphicon, InputGroup, Button,
  Panel,
} from 'react-bootstrap'
import { BreadCrumbs } from './BreadCrumbs'
import { ListTab } from './ListTab'
import styles from '../../../styles/images.scss'

const propTypes = {
  // structure attribute
  page: PropTypes.string.isRequired,

  // data status
  isFetching: PropTypes.bool.isRequired,

  // paging attributes
  p: PropTypes.number.isRequired,
  pageRows: PropTypes.number.isRequired,
  q: PropTypes.any.isRequired,

  // data
  item: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
  itemsCount: PropTypes.number.isRequired,

  // paging output
  setP: PropTypes.func.isRequired,
  setPageRows: PropTypes.func.isRequired,
  setQ: PropTypes.func.isRequired,

  // navi actions
  setTitle: PropTypes.func.isRequired,

  // API call
  fetchImages: PropTypes.func.isRequired,

}

const defaultProps = {
  page: 'images',
  item: {},
  items: [],
  itemsCount: 0,
  p: 1,
  q: '',
  pageRows: 10,
  isFetching: false,
}

export class Images extends Component {
  constructor(props) {
    super(props)
    this.bindAll('handlePageRowsChange', 'handlePageChange', 'handleFilterChange',
    'handleFocus', 'clearFilter', 'handleRefreshClick')
  }

  componentDidMount() {
    this.props.setTitle('Images')
  }

  componentWillUnmount() {

  }

  handlePageChange(p) {
    this.props.setP(p)
  }

  handlePageRowsChange(e) {
    this.props.setPageRows(e)
  }

  handleFilterChange(e) {
    this.props.setQ(e.target.value)
  }

  handleFocus(event) {
    event.target.select()
  }

  clearFilter() {
    this.props.setQ('')
  }

  handleRefreshClick() {

  }

  bindAll(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this)
    })
  }

  render() {
    const { isFetching, items, itemsCount, p, q, pageRows } = this.props
    return (
      <Grid>
        <Helmet title='IMAGES' />
        <BreadCrumbs />
        <Row>
          <Col lg={12}>
            <Row>
              <Panel>
                <Col md={4}>
                  <Link className='btn btn-configure' to='/'>
                    <Glyphicon glyph='circle-arrow-left'  />
                  </Link>
                  <h3 className={styles.appHeader}>Images</h3>
                </Col>
                <Col mdOffset={6} md={2}>
                  <Button
                    className='pull-right'
                    bsStyle='primary'
                    disabled={isFetching}
                    onClick={!isFetching ? this.handleRefreshClick : null}
                  >
                    {isFetching ? 'Loading...' : 'Refresh'}
                  </Button>
                </Col>
              </Panel>
            </Row>
            <Row>
              <Panel>
                <Col md={4}>
                  <nav className='pull-left'>
                    <Pagination
                      hideDisabled
                      activePage={p}
                      itemsCountPerPage={pageRows}
                      totalItemsCount={itemsCount}
                      pageRangeDisplayed={5}
                      onChange={this.handlePageChange}
                      firstPageText={<i className='fa fa-backward' />}
                      lastPageText={<i className='fa fa-forward' />}
                      prevPageText={<i className='fa fa-caret-left' />}
                      nextPageText={<i className='fa fa-caret-right' />}
                      innerClass={classnames(styles.marginZero, 'pagination', 'pull-right')}
                    />
                  </nav>
                </Col>
                <Col md={5}>
                  <form className='form form-horizontal'>
                    <FormGroup>
                      <InputGroup>
                        <FormControl
                          name='q'
                          datatype='text'
                          type='search'
                          value={q}
                          placeholder='Search lists'
                          className='form-control input-md'
                          onChange={this.handleFilterChange}
                          onFocus={this.handleFocus}
                        />
                        <InputGroup.Addon>
                          <Glyphicon
                            glyph='erase'
                            onClick={this.clearFilter}
                          />
                        </InputGroup.Addon>
                      </InputGroup>
                    </FormGroup>
                  </form>
                </Col>
                <Col md={2} className='pull-right'>
                  Show {' '}
                  <DropdownButton
                    id='show-images-top'
                    onSelect={this.handlePageRowsChange}
                    title={pageRows}
                    key={pageRows}
                    bsSize='small'
                  >
                    <MenuItem eventKey={10}>10</MenuItem>
                    <MenuItem eventKey={25}>25</MenuItem>
                    <MenuItem eventKey={50}>50</MenuItem>
                    <MenuItem eventKey={100}>100</MenuItem>
                  </DropdownButton>
                  {' '} images
                </Col>
              </Panel>
            </Row>
            <Row>
              <Panel>
                <Col lg={12}>
                  <ListTab images={items}  />
                </Col>
              </Panel>
            </Row>
            <Row>
              <Panel>
                <Col md={4}>
                  <nav className='pull-left'>
                    <Pagination
                      hideDisabled
                      activePage={p}
                      itemsCountPerPage={pageRows}
                      totalItemsCount={itemsCount}
                      pageRangeDisplayed={5}
                      onChange={this.handlePageChange}
                      firstPageText={<i className='fa fa-backward' />}
                      lastPageText={<i className='fa fa-forward' />}
                      prevPageText={<i className='fa fa-caret-left' />}
                      nextPageText={<i className='fa fa-caret-right' />}
                      innerClass={classnames(styles.marginZero, 'pagination', 'pull-right')}
                    />
                  </nav>
                </Col>
                <Col mdOffset={5} md={2} className='pull-right'>
                  Show {' '}
                  <DropdownButton
                    id='show-images-bottom'
                    onSelect={this.handlePageRowsChange}
                    title={pageRows}
                    key={pageRows}
                    bsSize='small'
                  >
                    <MenuItem eventKey={10}>10</MenuItem>
                    <MenuItem eventKey={25}>25</MenuItem>
                    <MenuItem eventKey={50}>50</MenuItem>
                    <MenuItem eventKey={100}>100</MenuItem>
                  </DropdownButton>
                  {' '} images
                </Col>
              </Panel>
            </Row>
          </Col>
        </Row>
      </Grid>
    )
  }
}

Images.propTypes = propTypes
Images.defaultProps = defaultProps
export default Images
