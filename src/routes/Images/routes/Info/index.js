// import { injectReducer } from '../../../../store/reducers'
// import { userIsAuthenticated } from '../../modules/Auth'

// export const createRoutes = store => ({
export const createRoutes = () => ({
  path: ':listname/info',

  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      const Container = require('./containers/IndexContainer').default
      // const reducer = require('./modules/reducers').default
      // injectReducer(store, { key: 'applications', reducer })
      callback(null, Container)
    }, 'images.childs.info')
  },

})

export default createRoutes
