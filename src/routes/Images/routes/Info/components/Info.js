import React, { Component } from 'react'

import { obj } from '../../../../../modules/utils'

const propTypes = {

}
const defaultProps = {

}

export class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <h1>List id</h1>
    )
  }
}

Index.propTypes = propTypes
Index.defaultProps = defaultProps
export default Index
