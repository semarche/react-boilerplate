import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { Button, Col } from 'react-bootstrap'
import classnames from 'classnames'
import styles from '../styles/index.scss'

const propTypes = {
  setTitle: PropTypes.func.isRequired,
  setSignOut: PropTypes.func.isRequired,
  user: PropTypes.object,
}

const defaultProps = {}
export class Index extends Component {

  render() {
    this.props.setTitle('Sign Out')
    return (
      <div className={classnames('container-fluid', 'text-center', styles.Logout)}>
        <Helmet title='Sign out'/>
        <br />
        <Col xs={6} sm={6} md={6} lg={6} lgPush={3}>
          <Button
            bsStyle='primary'
            bsClass={classnames('rounded', 'btn')}
            onClick={() => {
              this.props.setSignOut()
            }}
          >
            Sign Out
          </Button>
        </Col>
      </div>
    )
  }
}

Index.propTypes = propTypes
Index.defaultProps = defaultProps
export default Index
