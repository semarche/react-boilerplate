import { injectReducer } from '../../store/reducers'

import { userIsAuthenticated } from '../../modules/Auth'

export default (store) => ({
  path: 'logout',
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
     and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
       dependencies for bundling   */
      const IndexContainer = require('./containers/IndexContainer').default
      const reducer = require('../Login/modules').default

      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'auth', reducer })

      /*  Return getComponent   */
      cb(null, userIsAuthenticated(IndexContainer))

      /* Webpack named bundle   */
    }, 'logout')
  },
})
