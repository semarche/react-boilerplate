import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { Panel } from 'react-bootstrap'

import config from '../../../config'

const propTypes = {
  setTitle: PropTypes.func.isRequired,
}

const defaultProps = {}

export class Help extends Component {

  componentDidMount() {
    this.props.setTitle('Help')
  }

  render() {
    return (
      <div className='container-fluid text-center'>
        <Helmet title='Help' />
        <h4>Help</h4>
        <Panel>
          <p>tricorder-ui-framework</p>
          <p>version: <b>{config.version}</b></p>
        </Panel>
        <Panel>
          <p>tricorder-ui-backend</p>
          <p>adr: <b>{config.backend}</b></p>
        </Panel>
      </div>
    )
  }
}

Help.propTypes = propTypes
Help.defaultProps = defaultProps
export default Help
