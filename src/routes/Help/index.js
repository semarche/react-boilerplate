// import { injectReducer } from '../../store/reducers'
// export const createRoutes = store => ({

export const createRoutes = () => ({
  path: 'help',

  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      const Help = require('./containers/HelpContainer').default
      // const reducer = require('./modules/Help').default
      // injectReducer(store, { key: 'help', reducer })
      callback(null, Help)
    }, 'help')
  },

})

export default createRoutes
