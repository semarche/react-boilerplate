// https://bl.ocks.org/mbostock/4339083

import React from 'react'
import * as d3 from 'd3'
// import classnames from 'classnames'
import init from './init-data.json'

const ord = [
  'application',
  'labels-stack',
  'labels-cluster',
  'labels-pod-template-hash',
  'name',
  'labels-app',
  'labels-aa-cdman',
  'labels-version']


const getObjectProperty = (object, stringKey) => {
  let latestValue = object
  stringKey.split('.').forEach(i => {
    latestValue = latestValue[i]
  })
  return latestValue
}

const topologyDataTransform = (array, keys) => {
  if (keys) {
    const order = JSON.parse(JSON.stringify(keys))
    const groups = array.reduce((accumulator, currentValue) => {
      const key = getObjectProperty(currentValue, keys[0]);
      (accumulator[key] = accumulator[key] || []).push(currentValue)
      return accumulator
    }, {})
    const obj = Object.keys(groups, order, order.shift())
    return obj.map(name => (order[0] ? { name, children: topologyDataTransform(groups[name], order) } : { name }))
  }
  return array
}

const data = topologyDataTransform(init, ord)

class Topology extends React.Component {
  componentDidMount() {
    const margin = { top: 20, right: 120, bottom: 20, left: 120 }
    const width = document.getElementById('topologyGraph').offsetWidth - margin.right - margin.left
    const height = 600 - margin.top - margin.bottom

    let i = 0
    const duration = 750
    const root = data.length ? data[0] : {}
    //const root = marketing

    const tree = d3.layout.tree()
      .size([height, width])

    const diagonal = d3.svg.diagonal().projection(d => [d.y, d.x])

    const svg = d3.select('svg')
      .attr('width', width + margin.right + margin.left)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', `translate(${margin.left},${margin.top})`)

    root.x0 = height / 2
    root.y0 = 0

    const collapse = d => {
      if (d.children) {
        d._children = d.children
        d._children.forEach(collapse)
        d.children = null
      }
    }

    root.children.forEach(collapse)
    // console.log(JSON.stringify(root, null, 2))

    d3.select(self.frameElement).style('height', '800px')

    const update = source => {
      // Compute the new tree layout.
      const nodes = tree.nodes(root).reverse()
      const links = tree.links(nodes)

      // Normalize for fixed-depth.
      nodes.forEach(d => { d.y = d.depth * 180 })

      // Update the nodes…
      const node = svg.selectAll('g.node')
        .data(nodes, d => d.id || (d.id = ++i))

      // Enter any new nodes at the parent's previous position.
      const nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr('transform', `translate(${source.y0}, ${source.x0})`)
        .on('click', click)

      nodeEnter.append('circle')
          .attr('r', 1e-6)
          .attr('cursor', 'pointer')
          .style('fill', d => (d._children ? 'lightsteelblue' : '#286090'))

      nodeEnter.append('text')
          .attr('x', d => (d.children || d._children ? -10 : 10))
          .attr('dy', ".35em")
          .attr('text-anchor', 'start')
          .attr('alignment-baseline', 'hanging')
          .text(d => d.name)
          .style('fill-opacity', 1e-6)

      // Transition nodes to their new position.
      const nodeUpdate = node.transition()
        .duration(duration)
        .attr('transform', d => (`translate(${d.y}, ${d.x})`))

      nodeUpdate.select('circle')
        .attr('r', 4.5)
        .style('fill', d => (d._children ? 'lightsteelblue' : '#286090'))

      nodeUpdate.select('text')
        .style('fill-opacity', 1)

      // Transition exiting nodes to the parent's new position.
      const nodeExit = node.exit().transition()
        .duration(duration)
        .attr('transform', `translate(${source.y}, ${source.x})`)
        .remove()

      nodeExit.select('circle')
        .attr('r', 1e-6)

      nodeExit.select('text')
        .style('fill-opacity', 1e-6)

      // Update the links…
      const link = svg.selectAll('path.link')
        .data(links, d => d.target.id)

      // Enter any new links at the parent's previous position.
      link.enter().insert('path', 'g')
        .attr('class', 'link')
        .attr('d', () => {
          const o = { x: source.x0, y: source.y0 }
          return diagonal({ source: o, target: o })
        })

      // Transition links to their new position.
      link.transition()
        .duration(duration)
        .attr('d', diagonal)

      // Transition exiting nodes to the parent's new position.
      link.exit().transition()
        .duration(duration)
        .attr('d', () => {
          const o = { x: source.x, y: source.y }
          return diagonal({ source: o, target: o })
        })
        .remove()

      // Stash the old positions for transition.
      nodes.forEach(d => {
        d.x0 = d.x
        d.y0 = d.y
      })
    }

    // Toggle children on click.
    const click = d => {
      if (d.children) {
        d._children = d.children
        d.children = null
      } else {
        d.children = d._children
        d._children = null
      }
      update(d)
    }
    update(root)
  }

  render() {
    return <svg />
  }
}

export default Topology
