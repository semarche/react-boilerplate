import React from 'react'
import Helmet from 'react-helmet'
import { Tabs, Tab, Grid, Panel, Col, Row } from 'react-bootstrap'
import { BreadCrumbs } from './BreadCrumbs'
import Topology from './Topology'

const propTypes = {}
const defaultProps = {}

export const Marketing = (props) => {
  return (
    <Grid>
      <Helmet title='Marketing' />
      <BreadCrumbs />
      <Row>
        <Tabs defaultActiveKey={1} id='Monitoring-page-tabs'>
          <Tab eventKey={1} title='Topology'>
            <Panel id='topologyGraph'>
              <Topology />
            </Panel>
          </Tab>
        </Tabs>

      </Row>
    </Grid>
  )
}

Marketing.propTypes = propTypes
Marketing.defaultProps = defaultProps
export default Marketing
