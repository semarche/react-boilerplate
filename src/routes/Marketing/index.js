// import { injectReducer } from '../../../../store/reducers'
// import { userIsAuthenticated } from '../../modules/Auth'

// export const createRoutes = store => ({
export const createRoutes = () => ({
  path: 'topology',

  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      const Container = require('./containers/MarketingContainer').default
      // const reducer = require('./modules/reducers').default
      // injectReducer(store, { key: 'topology', reducer })
      callback(null, Container)
    }, 'topology')
  },

})

export default createRoutes
