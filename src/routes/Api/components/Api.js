import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { Panel, Col, Grid, Row } from 'react-bootstrap'
import PropTypes from 'prop-types'
import SwaggerUi, { presets } from 'swagger-ui'
import { differenceWith, isEqual } from 'lodash'
// import 'swagger-ui/dist/swagger-ui.css'
import 'swagger-ui-themes/themes/3.x/theme-feeling-blue.css'

import classnames from 'classnames'
import styles from '../styles/styles.scss'
import { obj, arr } from '../../../modules/utils'

import { BreadCrumbs } from './BreadCrumbs'
import api from '../../../config/api.json'

const propTypes = {
  gates: PropTypes.array.isRequired,
}
const defaultProps = {
  gates: [],
}

export class Api extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.initSwagger = (host) => SwaggerUi({
      dom_id: '#swaggerContainer',
      spec: { ...api, host: host.replace(/^https?:\/\//, '') },
      presets: [presets.apis],
    })
  }

  componentDidMount() {
    const host = arr.getActive(this.props.gates, 'address')
    this.initSwagger(host)
  }

  componentDidUpdate(prevProps) {
    if (differenceWith(prevProps.gates, this.props.gates, isEqual).length) {
      const host = arr.getActive(this.props.gates, 'address')
      this.initSwagger(host)
    }
  }

  render() {
    return (
      <Grid>
        <Helmet title='Api' />
        <BreadCrumbs />
        <Row>
          <Panel>
            <Col lg={12}>
              <div id='swaggerContainer' />
            </Col>
          </Panel>
        </Row>
      </Grid>
    )
  }
}

Api.propTypes = propTypes
Api.defaultProps = defaultProps
export default Api
