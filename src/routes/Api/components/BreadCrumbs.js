import React from 'react'
import { Breadcrumb } from 'react-bootstrap'
import { IndexLinkContainer } from 'react-router-bootstrap'

export const BreadCrumbs = () => (
  <Breadcrumb>
    <IndexLinkContainer to='/'>
      <Breadcrumb.Item href='/'>
        Dashboard
      </Breadcrumb.Item>
    </IndexLinkContainer>
    <Breadcrumb.Item active>
      Api
    </Breadcrumb.Item>
  </Breadcrumb>
)

export default BreadCrumbs
