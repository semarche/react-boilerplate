import environments from './environments'
import { str } from '../../../modules/utils'
// ========================================================
// Default Configuration
// ========================================================
const config = {
  env: process.env.NODE_ENV || 'development',
  dataLive: false,
  apihost: '/api',
  backend: {
    fetchData: str.template`${'host'}/some_url?count=${'count'}&q=${'query'}`,
  },
  method: {
    fetchData: 'GET',
  },
}

// ========================================================
// Environment Configuration
// ========================================================
const overrides = environments[config.env]
if (overrides) {
  Object.assign(config, overrides(config))
}

export default config
