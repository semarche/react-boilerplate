import { injectReducer } from '../../store/reducers'
// import { userIsAuthenticated } from '../../modules/Auth'

export const createRoutes = store => ({
  path: 'api',

  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      const Container = require('./containers/ApiContainer').default
      const consts = require('./modules/consts').default
      const reducer = require('./modules/reducers').default

      injectReducer(store, { key: consts.KEY, reducer })
      callback(null, Container)
    }, 'api')
  },

})

export default createRoutes
