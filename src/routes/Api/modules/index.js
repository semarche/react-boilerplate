import _ from 'lodash'
import { CALL_API } from 'redux-api-middleware'

import appsConfig from '../config'

import _consts from './consts'
import _actions from './actions'
// ------------------------------------
// Constants
// ------------------------------------
export const consts = _consts

// ------------------------------------
// Actions
// ------------------------------------
export const actions = _actions

// ------------------------------------
// Action Handlers
// ------------------------------------
export actionHandler from './reducers'

// ------------------------------------
// Reducer
// ------------------------------------
import initial from './initial'

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
