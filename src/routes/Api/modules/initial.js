// ------------------------------------
// Initial State
// ------------------------------------
export const initial = {
  page: 'api',
  gates: [
    { active: 'true', address: 'http://127.0.0.1', name: 'default', id: 'default' },
  ],
}

export default initial
