import { arr } from '../../../modules/utils'
// ------------------------------------
// Constants
// ------------------------------------
import { consts } from './consts'

// ------------------------------------
// Initial State
// ------------------------------------
import initialState from './initial'

// ------------------------------------
// Action Handlers
// ------------------------------------
export const actionHandlers = {
  [consts.CHANGE_DOCS_PAGE]: (state, action) => ({ ...state, page: action.payload }),
  [consts.SET_ID]: (state, action) => ({ ...state, id: action.payload }),
  [consts.SET_Q]: (state, action) => ({ ...state, q: action.payload, p: 1 }),
  [consts.SET_P]: (state, action) => ({ ...state, p: action.payload }),
  [consts.SET_PAGE_ROWS]: (state, action) => ({ ...state, p: 1, pageRows: action.payload }),

  [consts.FETCH_IMAGES_REQUEST]: state => ({ ...state, isFetching: true }),
  [consts.FETCH_IMAGES_FAILURE]: state => ({ ...state, isFetching: false }),
  [consts.FETCH_IMAGES_SUCCESS]: (state, action) => {
    const result = { ...state, isFetching: false }
    if (arr.isArray(action.payload)) {
      result.images = action.payload
    }
    return result
  },
  [consts.CACHE_FETCHING]: state => ({ ...state, isFetching: true }),

  [consts.FETCH_IMAGE_INFO_REQUEST]: state => ({ ...state, isFetching: true }),
  [consts.FETCH_IMAGE_INFO_FAILURE]: state => ({ ...state, isFetching: false }),
  [consts.FETCH_IMAGE_INFO_SUCCESS]: (state, action) => {
    const result = { ...state, isFetching: false }
    if (action.payload) {
      result.imageInfo = action.payload
    }
    return result
  },
}

export const reducers = (state = initialState, action) => {
  const handler = actionHandlers[action.type]
  return handler ? handler(state, action) : state
}

export default reducers
