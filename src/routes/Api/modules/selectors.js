import { createSelector } from 'reselect'
import { arr, obj } from '../../../modules/utils'
import {consts} from './consts'

export const getGates = state => obj.deepGet(state, [consts.KEY, 'gates'], 0)

export default {
  getGates,
}
