// We only need to import the modules necessary for initial render
export const createRoutes = store => ({
  path: '/',

  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      const inCoreLayout = require('../layouts/CoreLayout/CoreLayout').default
      callback(null, inCoreLayout)
    }, 'layout')
  },

  /**
   * or connect with home container
   *
   * @param partialNextState
   * @param callback
   */
  getIndexRoute(partialNextState, callback) {
    require.ensure([], (require) => {
      callback(null, {
        onEnter: (nextState, replace) => replace('/images'),
        component: require('./Home/components/Index').default,
      })
    }, 'home')
  },

  /**
   * hardcoded redirect
   */
  // indexRoute: {
  //   onEnter: (nextState, replace) => replace('/monuments'),
  // },

  getChildRoutes(partialNextState, callback) {
    require.ensure([], (require) => {
      callback(null, [
        require('./Help').default(store),
        require('./Marketing').default(store),
        require('./Api').default(store),
        require('./Images').default(store),
      ])
    }, 'root.childs')
  },

})

export default createRoutes
