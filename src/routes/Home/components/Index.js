import React from 'react'
import { Grid, Row, Col, Panel, ListGroup, ListGroupItem } from 'react-bootstrap'
import { Link } from 'react-router'

import styles from '../styles/dashboard.scss'

export const Index = () => (
  <Grid fluid>
    <Row>
      <Col lg={2} md={3} sm={4} xs={4}>
        <Link to='/applications' className={styles.link}>
          <Panel bsStyle='info' header='VNF Deployed' className={styles.pointer}>
            <ListGroup fill>
              <ListGroupItem header='Total Count' className={styles.bigText}>30</ListGroupItem>
            </ListGroup>
          </Panel>
        </Link>
      </Col>
      <Col lg={2} md={3} sm={4} xs={4}>
        <Link to='/applications' className={styles.link}>
          <Panel bsStyle='info' header='Containers Deployed' className={styles.pointer}>
            <ListGroup fill>
              <ListGroupItem header='Total Count' className={styles.bigText}>610</ListGroupItem>
            </ListGroup>
          </Panel>
        </Link>
      </Col>
      <Col lg={2} md={3} sm={4} xs={4}>
        <Link to='/applications' className={styles.link}>
          <Panel bsStyle='danger' header='Pipelines Failures' className={styles.pointer}>
            <ListGroup fill>
              <ListGroupItem header='Total Count' className={styles.bigText}>2</ListGroupItem>
            </ListGroup>
          </Panel>
        </Link>
      </Col>

      <Col lg={2} md={3} sm={4} xs={4}>
        <Link to='/applications' className={styles.link}>
          <Panel bsStyle='info' header='Number of pipelines' className={styles.pointer}>
            <ListGroup fill>
              <ListGroupItem header='Total Count' className={styles.bigText}>120</ListGroupItem>
            </ListGroup>
          </Panel>
        </Link>
      </Col>

      <Col lg={2} md={3} sm={4} xs={4}>
        <Link to='/applications' className={styles.link}>
          <Panel bsStyle='warning' header='Critical Updates' className={styles.pointer}>
            <ListGroup fill>
              <ListGroupItem header='Total Count' className={styles.bigText}>2</ListGroupItem>
            </ListGroup>
          </Panel>
        </Link>
      </Col>
    </Row>

  </Grid>
)

export default Index
