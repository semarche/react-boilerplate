import { push } from 'react-router-redux'

// ------------------------------------
// Constants
// ------------------------------------
export const consts = {
  SET_TITLE: 'HOME.SET_TITLE',
  REDIRECT: 'HOME.REDIRECT',
}

// ------------------------------------
// Actions
// ------------------------------------
export const goTo = url => dispatch => new Promise(resolve => setTimeout(() => {
  dispatch(push(url))
  dispatch({
    type: consts.REDIRECT,
  })
  // resolve()
}, 20))

export const setTitle = value => dispatch => new Promise(resolve => setTimeout(() => {
  dispatch({
    type: consts.SET_TITLE,
    payload: value,
  })
  // resolve()
}, 20))

export const actions = {
  goTo,
  setTitle,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [consts.SET_TITLE]: (state, action) => ({...state, title: action.payload}),
  [consts.REDIRECT]: state => ({...state, redirect: ''}),

}

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {title: 'REACT BOILERPLATE', redirect: ''}
export const homeReducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
export default homeReducer
