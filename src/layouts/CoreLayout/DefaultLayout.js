import React from 'react'
import PropTypes from 'prop-types'

export const DefaultLayout = () => (
  <div>
    children
  </div>
)

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
}

export default DefaultLayout

