import React from 'react'
import PropTypes from 'prop-types'
import LoadingBar from 'react-redux-loading-bar'
import classnames from 'classnames'
import styles from '../../styles/basic.scss'

export const BasicLayout = ({ children }) => (
  <div className={classnames(styles.panelWrapper, 'vertical-center')}>
    <LoadingBar maxProgress={100} style={{ backgroundColor: '#70c0f0' }}/>
    {children}
  </div>
)

BasicLayout.propTypes = {
  children: PropTypes.element.isRequired,
}

export default BasicLayout

