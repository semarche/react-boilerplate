import React from 'react'
import PropTypes from 'prop-types'
import LoadingBar from 'react-redux-loading-bar'
import { Grid, Row, Col } from 'react-bootstrap'
import HeaderContainer from '../../containers/Header/HeaderContainer'

import { LeftNavigationBar } from '../../components/LeftMenu/LeftNavigationBar'
import styles from './CoreLayout.scss'

export const CoreLayout = ({ children }) => (
  <Grid fluid id='wrapper'>
    <LoadingBar maxProgress={100} style={{ backgroundColor: '#70c0f0' }}/>
    <Row>
      <Col className={styles.pageWideCell}>
        <HeaderContainer />
      </Col>
    </Row>

    <Row style={{ minHeight: 600 }}>
      <div className={styles.pageContainer}>
        <div className={styles.pageRow}>
          <div className={styles.pageLeftCell}><LeftNavigationBar /></div>
          <div className={styles.pageCell}>
            {children}
          </div>
        </div>

      </div>
    </Row>

  </Grid>


)

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
}

export default CoreLayout

