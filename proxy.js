var koa = require('koa');
var proxy = require('koa-proxy');
var app = new koa();
app.use(proxy({
  host: 'http://localhost:8084/'
}));
app.listen(3001);
