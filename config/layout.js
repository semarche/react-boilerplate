// Default Helmet props
export default Object.freeze({
  htmlAttributes: { lang: 'en' },
  title: 'Deck',
  defaultTitle: 'Deck',
  titleTemplate: 'Deck | %s',
  meta: [
    { charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    {
      name: 'description',
      content: ''
    },
    { name: 'keywords', content: 'vnf, deployment, cloud' },
  ],
  link: [
    { rel: 'shortcut icon', href: '/favicon.ico' },
    { rel: 'stylesheet', href: '/assets/css/bootstrap.min.css' },
    { rel: 'stylesheet', href: '/assets/css/bootstrap-theme.min.css' },
    { rel: 'stylesheet', href: '/assets/font-awesome/css/font-awesome.css' },
    { rel: 'stylesheet', href: '/custom.1.css' },
  ],
  script: [],
  style: [],
})
