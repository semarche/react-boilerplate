
FROM node:7.5-alpine
MAINTAINER Marchenko Sergey <sergey.dnepro@gmail.com>

RUN apk update && apk add --update --no-cache alpine-sdk python && \
    python -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip install --upgrade pip setuptools && \
    rm -r /root/.cache

RUN mkdir -p /app
WORKDIR /app

COPY package.json /app/
RUN npm install --no-optional  --silent

ADD ./ /app

ENV HOST "0.0.0.0"
ENV PORT "3000"
ENV NODE_ENV "production"
EXPOSE 3000

RUN npm run robocss && npm run bootstrap && npm run deploy:prod
CMD ["npm", "run", "start:prod"]
