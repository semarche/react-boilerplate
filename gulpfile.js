var gulp = require('gulp');
var lessToScss = require('gulp-less-to-scss');
var sass = require('gulp-sass');
var less = require('gulp-less');
var path = require('path');
var sourcemaps = require('gulp-sourcemaps');
var concatCss = require('gulp-concat-css');

gulp.task("createFileIndex", function() {
  var index = gulp.src(['*.less']);
  console.log("INDEX:", index);
});

gulp.task('lessToScss', function() {
  gulp.src('app/scripts/**/*.less')
    .pipe(lessToScss())
    .pipe(gulp.dest('build/scss'));
});

gulp.task('sass', function() {
  return gulp.src('build/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('build/css'));
});

gulp.task('bootToScss', function() {
  gulp.src('bootstrap/**/*.less')
    .pipe(lessToScss())
    .pipe(gulp.dest('bootstrap/scss'));
});

gulp.task('less', function() {
  return gulp.src('app/scripts/**/*.less')
    .pipe(sourcemaps.init())
    .pipe(less({
      paths: [path.join(__dirname, './')]
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/css'));
});

gulp.task('css', function () {
  return gulp.src('./build/css/**/*.css')
    .pipe(concatCss('styles/bundle.css'))
    .pipe(gulp.dest('build/'));
});
