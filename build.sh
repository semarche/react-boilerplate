#!/bin/sh

date "+%m/%d %H:%M:%S"
echo "+ '$ARTIFACT_VERSION'" >> config/version.js
echo "building '$ARTIFACT_VERSION' version"
#npm install
#npm run robocss
#npm run bootstrap
#npm run deploy:prod
#Date "+%m/%d %H:%M:%S"
#export ARTIFACT_VERSION=`grep "^ARTIFACT_VERSION=" version.properties | awk -F  "=" '{print $2}'`
tar -czvf tricorder-ui-framework.$ARTIFACT_VERSION.tar.gz ./dist
#tar -czf ui-webframework.$ARTIFACT_VERSION.tgz --exclude *.tgz * .babelrc .eslintrc .editorconfig .npmignore
#Date "+%m/%d %H:%M:%S"
#npm run lint
#npm run test
